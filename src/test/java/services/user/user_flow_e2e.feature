# language:en
# encoding UTF-8

@e2e_user
Feature: User flow

  Scenario Outline: Create an user, update it and then delete it
    * def data_user = {name: <name_value>, email: <email_value>, gender: <gender_value>, status: <status_value>}

    #create an user
    * def user = call read('classpath:services/user/user.feature@e2e_step_create') data_user
    * set data_user.id = user.response.data.id

    #verify if create was successful
    * def check_user = call read('classpath:services/user/user.feature@e2e_step_get_id_with_data') {value_search: #(data_user.id)}
    Then match check_user.response.data[0] contains data_user

#    #update an user
#    * set data_user.name = <new_name>
#    * def update_user = call read('classpath:services/user/user.feature@e2e_step_update') data_user
#
#    #verify if update was successful
#    * def check_user = call read('classpath:services/user/user.feature@e2e_step_get_id') {value_search: #(data_user.id)}
#    Then match check_user.response.data contains data_user
#
#    #delete an user
#    * call read('classpath:services/user/user.feature@e2e_step_delete') {value_search: #(data_user.id)}
#
#    #verify if deletion was successful
#    * def check_user = call read('classpath:services/user/user.feature@e2e_step_get_id_without_data') {value_search: #(data_user.id)}
#    Then check_user.response.data = []

    Examples:
      | name_value       | email_value                 | gender_value | status_value | new_name         |
      | 'Kelly Mag Test' | 'kelly_mag_teste@gmail.com100000w0w0' | 'female'     | 'active'     | 'Kelly Test Mag' |