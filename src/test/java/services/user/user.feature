@user_controller
Feature: User controller

  Background:
    * def data = read('classpath:services/user/data/'+ env +'.yaml')
    * def support_data = read('classpath:services/support/data/data.yaml')
    * def authorization = support_data.user.authorization

  @get_all_user @positive
  Scenario Outline: Get all user on /public-api/users with <expected_response>
    Given url url_public_api
    And path '/public-api/users'
    And header Authorization = authorization
    And params <parameters>
    And request {}
    When method get
    Then status 200
    And match response contains data.user.message.positive.get_all.<contract>
    And match response.meta.pagination contains data.user.message.positive.get_all.pagination
    And match each response.data contains data.user.message.positive.get_all.<expected_response>

    @e2e_step_get_id_with_data @skip_ci
    Examples:
      | parameters              | expected_response | contract           |
      | {id: "#(value_search)"} | search_by_id      | contract_with_data |

    @e2e_step_get_id_without_data @skip_ci
    Examples:
      | parameters              | expected_response | contract              |
      | {id: "#(value_search)"} | search_by_id      | contract_without_data |

    @e2e_step_get_all_email @skip_ci
    Examples:
      | parameters                 | expected_response | contract           |
      | {email: "#(value_search)"} | search_by_email   | contract_with_data |

    @get_all
    Examples:
      | parameters | expected_response | contract           |
      | {}         | no_filters        | contract_with_data |

  @get_user
  Scenario Outline: Get a user on /public-api/users/<search_by>
    * def value_search = karate.get('value_search',<search_by>)
    Given url url_public_api
    And path '/public-api/users',value_search
    And header Authorization = authorization
    And request {}
    When method get
    Then status 200
    And match response.data contains <expected_response>

    @positive @e2e_step_get_id @skip_ci
    Examples:
      | search_by | expected_response                               |
      | null      | data.user.message.positive.get_all.search_by_id |

    @negative
    Examples:
      | search_by | expected_response                              |
      | 0         | {"message": "#(data.user.message.not_found)" } |

  @create_user @positive @e2e_step_create @skip_ci
  Scenario: Create a new user on /public-api/users with valid information and code <code>
    * def body = read('classpath:services/user/payload/user.json')
    Given url url_public_api
    And path '/public-api/users'
    And header Authorization = authorization
    And request body
    When method post
    Then status 200
    And match response contains {code: 201}
    And match response.data contains data.user.object_user

  @create_user
  Scenario Outline: Create a new user on /public-api/users with <field> is <text> and code <code>
    * def name = karate.get('name', <name_value>)
    * def email = karate.get('email',<email_value>)
    * def gender = karate.get('gender',<gender_value>)
    * def status = karate.get('status',<status_value>)
    * def body = read('classpath:services/user/payload/user.json')
    Given url url_public_api
    And path '/public-api/users'
    And header Authorization = authorization
    And request body
    When method post
    Then status 200
    And match response contains {code: <code>}
    And match response.data contains {"field": <field>,"message": #(data.user.message.<text>)}

    @negative
    Examples:
      | name_value  | email_value             | gender_value | status_value | field    | text           | code |
      | 'Kelly Mag' | 'kelly_teste@gmail.com' | 'Female'     | 'Active'     | 'email'  | already_taken  | 422  |
      | 'Kelly Mag' | 'kelly.teste'           | 'Female'     | 'Active'     | 'email'  | invalid        | 422  |
      | 'Kelly Mag' | ''                      | 'Female'     | 'Active'     | 'email'  | blank          | 422  |
      | ''          | 'kmag_teste@gmail.com'  | 'Female'     | 'Active'     | 'name'   | blank          | 422  |
      | 'Kelly Mag' | 'kmag_teste@gmail.com'  | ''           | 'Active'     | 'gender' | blank          | 422  |
      | 'Kelly Mag' | 'kmag_teste@gmail.com'  | 'female'     | 'Active'     | 'gender' | gender_invalid | 422  |
      | 'Kelly Mag' | 'kmag_teste@gmail.com'  | 'Female'     | ''           | 'status' | blank          | 422  |
      | 'Kelly Mag' | 'kmag_teste@gmail.com'  | 'Female'     | 'active'     | 'status' | status_invalid | 422  |

  @update_user
  Scenario Outline: Update a user with new name on /public-api/users
    * def id = karate.get('id', <id_value>)
    * def name = karate.get('name', <name_value>)
    * def email = karate.get('email',<email_value>)
    * def gender = karate.get('gender',<gender_value>)
    * def status = karate.get('status',<status_value>)
    * def body = read('classpath:services/user/payload/user.json')
    Given url url_public_api
    And path '/public-api/users', id
    And header Authorization = authorization
    And request body
    When method put
    Then status 200
    And match response contains <expected_response>

    @positive @e2e_step_update @skip_ci
    Examples:
      | id_value | name_value  | email_value             | gender_value | status_value | expected_response                                            |
      | null     | 'Kelly Mag' | 'kelly_teste@gmail.com' | 'Female'     | 'Active'     | {"code":200,"meta":null,"data": "#(data.user.object_user)" } |

    @negative
    Examples:
      | id_value | name_value  | email_value             | gender_value | status_value | expected_response                                                             |
      | 0        | 'Kelly Mag' | 'kelly_teste@gmail.com' | 'Female'     | 'Active'     | {"code":404,"meta":null,"data":{"message": "#(data.user.message.not_found)"}} |

  @delete_user @e2e_step_delete @positive @skip_ci
  Scenario: Delete a user on /public-api/users/ with valid id
    Given url url_public_api
    And path '/public-api/users',value_search
    And header Authorization = authorization
    And request {}
    When method delete
    Then status 200
    And match response contains {code: 204}

  @delete_user
  Scenario Outline: Delete a user on /public-api/users/ with invalid id
    * def value_search = karate.get('value_search',<search_by>)
    Given url url_public_api
    And path '/public-api/users',value_search
    And header Authorization = authorization
    And request {}
    When method delete
    Then status 200
    And match response.data.message contains data.user.message.not_found

    @negative
    Examples:
      | search_by |
      | 0         |