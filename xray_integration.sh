#!/bin/sh

client_id=$1
client_secret=$2
testPlanId=$3

token_xray=$(curl -H "Content-Type: application/json" -X POST --data "{ \"client_id\": \"$client_id\",\"client_secret\": \"$client_secret\" }" https://xray.cloud.xpand-it.com/api/v1/authenticate| tr -d '"')
for i in `ls target/surefire-reports/services*.xml`;
do
  curl -H "Content-Type: text/xml" -X POST -H "Authorization: Bearer $token_xray" --data "@$i"  "https://xray.cloud.xpand-it.com/api/v1/import/execution/junit?projectKey=PPAY&testPlanKey=$testPlanId";
done